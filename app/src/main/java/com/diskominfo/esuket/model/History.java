package com.diskominfo.esuket.model;

import android.graphics.drawable.Drawable;

public class History {

    public int id;
    public Integer image = null;
    public Drawable imageDrw;
    public String from;
    public String email;
    public String message;
    public String message2;
    public String date;
    public int color = -1;


}