package com.diskominfo.esuket.model;

import android.graphics.drawable.Drawable;

public class People {

    public int image;
    public Drawable imageDrw;
    public String name;
    public String descript;
    public String email;
    public boolean section = false;

    public People() {
    }

    public People(String name, boolean section) {
        this.name = name;
        this.section = section;
    }

    public People(String name, String descript, boolean section){
        this.name = name;
        this.descript = descript;
        this.section = section;
    }



}
