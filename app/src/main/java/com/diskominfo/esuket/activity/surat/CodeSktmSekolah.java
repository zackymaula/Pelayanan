package com.diskominfo.esuket.activity.surat;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeMediaHelper;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.utils.ViewAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class CodeSktmSekolah extends AppCompatActivity {

    CodeSharedPreferenceHelper csp;
    Context context;
    private TextView mtext, textViewJenisKelamin;
    EditText editDipergunakan,
            editNikAnak,
            editTempatLahirAnak,
            editTanggalLahirAnak,
            editNamaSekolah,
            editKelasSemesterSekolah,
            editAlamatSekolah;
    Button btnAjukanSurat, spnHubungan;
    ProgressDialog pDialog;

    //picture
    CodeMediaHelper codeMediaHelper;
    private static final int RC_CAMERA_CAPTURE =100;
    private static final int RC_PHOTO_ALBUM = 101;
    Uri fileUri;
    String imageString = "";
    ImageView imgViewUpload;

    String str_anak_nama="",str_anak_jenis_klmn="",str_anak_tmpt_lhr="",str_anak_tgl_lhr="", str_anak_nik="",
            str_hubungan="";
    private String[] array_hubOrtu;

    int cek =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sktm_sekolah);

        array_hubOrtu = getResources().getStringArray(R.array.HbngnOrtu);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Tunggu ...");
        pDialog.setCancelable(false);

        spnHubungan = (Button)findViewById(R.id.spn_hubortu);
        editNikAnak = (EditText)findViewById(R.id.editTextSktmSekolahNik);
        editTempatLahirAnak = (EditText)findViewById(R.id.editTextSktmSekolahTempatLahir);
        editTanggalLahirAnak = (EditText)findViewById(R.id.editTextSktmSekolahTanggalLahir);
        editNamaSekolah = (EditText)findViewById(R.id.editTextSktmSekolahNamaSekolah);
        editKelasSemesterSekolah = (EditText)findViewById(R.id.editTextSktmSekolahKelasSemester);
        editAlamatSekolah = (EditText)findViewById(R.id.editTextSktmSekolahAlamatSekolah);
        editDipergunakan = (EditText)findViewById(R.id.editTextSktmSekolahDipergunakan);
        textViewJenisKelamin = (TextView)findViewById(R.id.textViewSktmSekolahJenisKelamin);
        btnAjukanSurat = (Button)findViewById(R.id.buttonSktmSekolahAjukanSurat);
        imgViewUpload = (ImageView)findViewById(R.id.imageViewSktmSekolah);

        btnAjukanSurat.setOnClickListener(btnClick);
        ((FloatingActionButton) findViewById(R.id.fab_nik_sktm)).setOnClickListener(btnClick);
        ((FloatingActionButton) findViewById(R.id.fab_upload_gambar)).setOnClickListener(btnClick);

        initComponent();
        SetText();
    }
    private void initComponent(){
        mtext = (TextView) findViewById(R.id.textViewActionbarForm);
        mtext.setText("SKTM Sekolah");

        ((ImageButton) findViewById(R.id.buttonActionBarBackForm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((Button) findViewById(R.id.spn_hubortu)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHubDialog((Button)v);
            }
        });
    }

    private void showHubDialog(final Button bt){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(array_hubOrtu, cek, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_hubOrtu[which]);
                str_hubungan = array_hubOrtu[which];
                cek=which;
            }
        });
        builder.show();
    }

    public void SetText(){
        ((TextView) findViewById(R.id.TextView_SktmSklhNama)).setText(csp.getNama());
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.fab_nik_sktm:
                    showProgressDialog();
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            CodeConfig.URL_NIK + editNikAnak.getText().toString().trim(),
                            null,listenerGetAnak,jsonObjectErrorListener);
                    Volley.newRequestQueue(CodeSktmSekolah.this).add(jsonObjectRequest);
                    break;
                case R.id.fab_upload_gambar:
                    dialogUploadGambar();
                    break;
                case R.id.buttonSktmSekolahAjukanSurat:
                    if (editNikAnak.getText().toString().equals("")||
                            editTempatLahirAnak.getText().toString().equals("")||
                            editTanggalLahirAnak.getText().toString().equals("")||
                            textViewJenisKelamin.getText().toString().equals("")||
                            editNamaSekolah.getText().toString().equals("")||
                            editKelasSemesterSekolah.getText().toString().equals("")||
                            editAlamatSekolah.getText().toString().equals("")||
                            editDipergunakan.getText().toString().equals("")||
                            str_hubungan == ""||
                            imageString == ""
                            ) {
                        Toast.makeText(getBaseContext(),"Mohon pengisian dilengkapi",Toast.LENGTH_LONG).show();
                    } else {
                        prosesUpload();
                    }
                    break;
            }
        }
    };

    Response.Listener<JSONObject> listenerGetAnak = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {
                JSONArray item = response.getJSONArray("item");
                JSONObject person = item.getJSONObject(0);

                str_anak_nama = person.getString("NAMA_LGKP");
                str_anak_tmpt_lhr = person.getString("TMPT_LHR");
                str_anak_jenis_klmn = person.getString("JENIS_KLMN");
                str_anak_tgl_lhr = person.getString("TGL_LHR");
                str_anak_nik = person.getString("NIK");

                editNikAnak.setText(str_anak_nama);
                editTempatLahirAnak.setText(str_anak_tmpt_lhr);
                editTanggalLahirAnak.setText(str_anak_tgl_lhr);

                if (str_anak_jenis_klmn.equals("Laki-Laki")) {
                    textViewJenisKelamin.setText("Jenis Kelamin : Laki-Laki");
                    str_anak_jenis_klmn = "1";
                } else {
                    textViewJenisKelamin.setText("Jenis Kelamin : Perempuan");
                    str_anak_jenis_klmn = "2";
                }

                dismissProgressDialog();

            } catch (JSONException e) {
                e.printStackTrace();
                dismissProgressDialog();
                Toast.makeText(getBaseContext(),"Maaf, NIK tidak terdaftar",Toast.LENGTH_LONG).show();
            }
        }
    };
    Response.ErrorListener jsonObjectErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            dismissProgressDialog();
            Toast.makeText(getBaseContext(),"Tidak bisa terhubung",Toast.LENGTH_LONG).show();
        }
    };

    //menampilkan progress dialog
    void showProgressDialog(){
        if(!pDialog.isShowing())pDialog.show();
    }

    //menutup progress dialog
    void dismissProgressDialog(){
        if(pDialog.isShowing())pDialog.dismiss();
    }

    public void prosesUpload(){
        showProgressDialog();
        StringRequest requestUpload = new StringRequest(Request.Method.POST, CodeConfig.URL_INSERT_SKTM_SEKOLAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissProgressDialog();
                        spnHubungan.setText("Hubungan");
                        editNikAnak.setText("");
                        editTempatLahirAnak.setText("");
                        editTanggalLahirAnak.setText("");
                        textViewJenisKelamin.setText("Jenis Kelamin");
                        editNamaSekolah.setText("");
                        editKelasSemesterSekolah.setText("");
                        editAlamatSekolah.setText("");
                        editDipergunakan.setText("");
                        str_hubungan = "";
                        str_anak_nik = "";
                        str_anak_nama = "";
                        str_anak_jenis_klmn = "";
                        str_anak_tmpt_lhr = "";
                        str_anak_tgl_lhr = "";
                        imageString = "";
                        imgViewUpload.setImageResource(R.drawable.ic_crop_original);
                        Toast.makeText(getBaseContext(),"Berhasil upload data",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Gagal upload data",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<>();
                params.put("nik",csp.getNik());
                //params.put("nik","3571011202940002");
                //params.put("nama_lgkp",csp.getNama());
                //params.put("tmpt_lhr",csp.getTmpt_lhr());
                //params.put("tgl_lhr",csp.getTgl_lhr());
                //params.put("jenis_klmn",csp.getKelamin());
                //params.put("pekerjaan",csp.getPekerjaan());
                //params.put("alamat",csp.getAlamat());
                //params.put("rt",csp.getRt());
                //params.put("rw",csp.getRw());
                params.put("kelurahan",csp.getKel());
                //params.put("kecamatan",csp.getKec());
                //params.put("jnssurat","umum");
                //params.put("stat_kwn",csp.getStat_kwn());
                //params.put("agama",csp.getAgama());
                //params.put("pendidikan",csp.getPendidikan());
                params.put("id_instansi",csp.getNoKel()); //id_instansi
                params.put("jenis_sktm","2");
                params.put("nm_anak", str_anak_nama);
                params.put("tmp_lahir_anak", str_anak_tmpt_lhr);
                params.put("tgl_lahir_anak", str_anak_tgl_lhr);
                params.put("jenkel", str_anak_jenis_klmn);
                params.put("hubungan", str_hubungan);
                params.put("nm_sekolah", editNamaSekolah.getText().toString());
                params.put("kelas", editKelasSemesterSekolah.getText().toString());
                params.put("alamat_sekolah", editAlamatSekolah.getText().toString());
                params.put("kegunaan",editDipergunakan.getText().toString());

                params.put("image_string",imageString);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestUpload);
    }

    public void dialogUploadGambar(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_surat_gambar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((FloatingActionButton)dialog.findViewById(R.id.fab_upload_camera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeMediaHelper = new CodeMediaHelper();
                fileUri = codeMediaHelper.getOutputMediaFileUri();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, RC_CAMERA_CAPTURE);
                }
                dialog.dismiss();
            }
        });

        ((FloatingActionButton)dialog.findViewById(R.id.fab_upload_gallery)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Pilih Gambar ."), RC_PHOTO_ALBUM);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == RC_CAMERA_CAPTURE) {
                imageString = codeMediaHelper.previewCapturedImage(imgViewUpload);
            }
            else if(requestCode == RC_PHOTO_ALBUM){
                CodeMediaHelper codeMediaHelper2 = new CodeMediaHelper(this,data.getData());
                imageString = codeMediaHelper2.previewCapturedImage(imgViewUpload);
            }
        }
    }
}
