package com.diskominfo.esuket.activity.assets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by clampben on 2/17/2018.
 */

public class CodeMediaHelper {

    Bitmap bmp, bitmap;
    Uri fileUri;
    String namaFileBitmap, imgStr;
    boolean fromAlbum;
    Context context;

    private static final int RC_CAMERA_CAPTURE =100;
    private static final int RC_PHOTO_ALBUM = 101;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "captured_images";

    public CodeMediaHelper(){
        this.fileUri = null;
        this.bitmap = null;
        this.imgStr = "";
        fromAlbum = false;
    }

    public CodeMediaHelper(Context context, Uri fileUri){
        this.fileUri = fileUri;
        this.context = context;
        this.bitmap = null;
        fromAlbum = true;
    }

    public Uri getOutputMediaFileUri() {
        fileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
        return fileUri;
    }

    private static File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Gagal membuat "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "CAM_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    public String getImgStr() {
        return imgStr;
    }

    public String previewCapturedImage(ImageView imageView) {
        try {
            if(fromAlbum == true) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(
                            context.getContentResolver(),fileUri);
                } catch (IOException e)  {
                    e.printStackTrace(); }
            }else{
                bitmap = BitmapFactory.decodeFile(fileUri.getPath()); }

            namaFileBitmap= UUID.randomUUID().toString().
                    replaceAll("-","").substring(1,14)+".jpg";
            int dim = 1280;
            if(bitmap.getHeight()<bitmap.getWidth()){
                bmp= Bitmap.createScaledBitmap(bitmap,
                        dim,(int)((bitmap.getHeight()*dim)/(double)bitmap.getWidth()),
                        true);
            }else{
                bmp= Bitmap.createScaledBitmap(bitmap,
                        (int)((bitmap.getWidth()*dim)/(double)bitmap.getHeight()),
                        dim,true);
            }

            imageView.setImageBitmap(bmp);
            return imgStr = bitmapToString(bmp);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return "GAGAL";
    }

    public String bitmapToString(Bitmap bmp){
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 70, os);
        byte[] bmpBytes = os.toByteArray();
        String encodedBmp = Base64.encodeToString(bmpBytes, Base64.DEFAULT);
        return encodedBmp;
    }

    public int getRcCameraCapture() {
        return RC_CAMERA_CAPTURE;
    }

    public int getRcPhotoAlbum() {
        return RC_PHOTO_ALBUM;
    }
}
