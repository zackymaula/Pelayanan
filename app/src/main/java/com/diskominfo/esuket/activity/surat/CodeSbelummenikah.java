package com.diskominfo.esuket.activity.surat;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeMediaHelper;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.utils.ViewAnimation;

import java.util.Hashtable;
import java.util.Map;

public class CodeSbelummenikah extends AppCompatActivity {

    CodeSharedPreferenceHelper csp;
    Context context;
    private TextView mtext;
    EditText editDipergunakan;
    Button btnAjukanSurat;
    ProgressDialog pDialog;

    //picture
    CodeMediaHelper codeMediaHelper;
    private static final int RC_CAMERA_CAPTURE =101;
    private static final int RC_PHOTO_ALBUM = 102;
    Uri fileUri;
    String imageString = "";
    ImageView imgViewUpload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sbelummenikah);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);

        editDipergunakan = (EditText)findViewById(R.id.editTextSbelumNikahDipergunakan);
        btnAjukanSurat = (Button)findViewById(R.id.buttonSbelumNikahAjukanSurat);
        imgViewUpload = (ImageView)findViewById(R.id.imageViewSbelumNikah);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Upload data ke server ...");
        pDialog.setCancelable(false);

        ((FloatingActionButton) findViewById(R.id.fab_upload_gambar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUploadGambar();
            }
        });

        initComponent();
        SetText();
        onClickAjukan();
    }
    private void initComponent(){
        mtext = (TextView) findViewById(R.id.textViewActionbarForm);
        mtext.setText("Surat Keterangan Belum Menikah");

        ((ImageButton) findViewById(R.id.buttonActionBarBackForm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void SetText(){
        ((TextView) findViewById(R.id.TextView_SblmNama)).setText(csp.getNama());
    }

    public void onClickAjukan(){
        btnAjukanSurat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editDipergunakan.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(),"Mohon mengisi dipergunakan untuk",Toast.LENGTH_LONG).show();
                } else if (imageString == "") {
                    Toast.makeText(getBaseContext(),"Belum ada foto Surat Pengantar",Toast.LENGTH_LONG).show();
                } else {
                    prosesUpload();
                }
            }
        });
    }

    //menampilkan progress dialog
    void showProgressDialog(){
        if(!pDialog.isShowing())pDialog.show();
    }

    //menutup progress dialog
    void dismissProgressDialog(){
        if(pDialog.isShowing())pDialog.dismiss();
    }

    public void prosesUpload(){
        showProgressDialog();
        StringRequest requestUpload = new StringRequest(Request.Method.POST, CodeConfig.URL_INSERT_SBELUMNIKAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissProgressDialog();
                        imageString = "";
                        editDipergunakan.setText("");
                        imgViewUpload.setImageResource(R.drawable.ic_crop_original);
                        Toast.makeText(getBaseContext(),"Berhasil upload data",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Gagal upload data",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<>();
                params.put("nik",csp.getNik());
                //params.put("nik","3571011202940002");
                //params.put("nama_lgkp",csp.getNama());
                //params.put("tmpt_lhr",csp.getTmpt_lhr());
                //params.put("tgl_lhr",csp.getTgl_lhr());
                //params.put("jenis_klmn",csp.getKelamin());
                //params.put("pekerjaan",csp.getPekerjaan());
                //params.put("alamat",csp.getAlamat());
                //params.put("rt",csp.getRt());
                //params.put("rw",csp.getRw());
                params.put("kelurahan",csp.getKel());
                //params.put("kecamatan",csp.getKec());
                //params.put("jnssurat","umum");
                //params.put("stat_kwn",csp.getStat_kwn());
                //params.put("agama",csp.getAgama());
                //params.put("pendidikan",csp.getPendidikan());
                params.put("id_instansi",csp.getNoKel()); //id_instansi
                params.put("kegunaan",editDipergunakan.getText().toString());
                params.put("image_string",imageString);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestUpload);
    }

    private void dialogUploadGambar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_surat_gambar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((FloatingActionButton)dialog.findViewById(R.id.fab_upload_camera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeMediaHelper = new CodeMediaHelper();
                fileUri = codeMediaHelper.getOutputMediaFileUri();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, RC_CAMERA_CAPTURE);
                }
                //startActivityForResult(intent, RC_CAMERA_CAPTURE);
                dialog.dismiss();
            }
        });

        ((FloatingActionButton)dialog.findViewById(R.id.fab_upload_gallery)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Pilih Gambar ."), RC_PHOTO_ALBUM);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == RC_CAMERA_CAPTURE){
                //CodeMediaHelper codeMediaHelper = new CodeMediaHelper(this,data.getData());
                imageString = codeMediaHelper.previewCapturedImage(imgViewUpload);
            }
            else if(requestCode == RC_PHOTO_ALBUM){
                CodeMediaHelper codeMediaHelper2 = new CodeMediaHelper(this,data.getData());
                imageString = codeMediaHelper2.previewCapturedImage(imgViewUpload);
            }
        }
    }
}
