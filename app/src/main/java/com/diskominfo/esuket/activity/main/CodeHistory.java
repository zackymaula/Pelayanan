package com.diskominfo.esuket.activity.main;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.adapter.AdapterListHistory;
import com.diskominfo.esuket.adapter.AdapterListInbox;
import com.diskominfo.esuket.data.DataGenerator;
import com.diskominfo.esuket.model.History;
import com.diskominfo.esuket.utils.Tools;
import com.diskominfo.esuket.widget.LineItemDecoration;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;



public class CodeHistory extends AppCompatActivity {

    private SwipeRefreshLayout swipe_layout;
    private TextView mtext;
    private View parent_view;

    private RecyclerView recyclerView;
    private AdapterListInbox mAdapter;
    private AdapterListHistory mAdapter2;
    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    private List<History> items2;

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottom_sheet;

    public static List<History> items = new ArrayList<>();
    Context mcont;

    private static CodeSharedPreferenceHelper codeSharedPreferenceHelper,csp;
    Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    int value_notif ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_history);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(this);

        parent_view = findViewById(R.id.lyt_parent);

        mcont = this;
        items.clear();
        getHistory();
        /*if(items.isEmpty()){
            getHistory();
        }else{
            Collections.sort(items, new Comparator<History>() {
                @Override
                public int compare(History o1, History o2) {
                    String date1 = o1.date.toUpperCase();
                    String date2 = o2.date.toUpperCase();
                    return date2.compareTo(date1);
                }
            });
            initlist();
        }*/

        setActionBar();

    }

    /*private void setActionBar(){
        mtext = (TextView) findViewById(R.id.textViewActionbarForm);
        mtext.setText("History Pengajuan Surat");

        ((ImageButton) findViewById(R.id.buttonActionBarBackForm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }*/

    private void setActionBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("History Pengajuan Surat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(getBaseContext(), CodeAccount.class));
            finish();
        } else if (id == R.id.action_sortbytanggal){
            //Toast.makeText(getApplicationContext(), "Sort by Tanggal", Toast.LENGTH_SHORT).show();
            Collections.sort(items, new Comparator<History>() {
                @Override
                public int compare(History o1, History o2) {
                    String date1 = o1.date.toUpperCase();
                    String date2 = o2.date.toUpperCase();
                    return date2.compareTo(date1);
                }
            });
            initlist();
        } else if (id == R.id.action_sortbystatus){
            //Toast.makeText(getApplicationContext(), "Sort by Status", Toast.LENGTH_SHORT).show();
            Collections.sort(items, new Comparator<History>() {
                @Override
                public int compare(History o1, History o2) {
                    return o2.image.compareTo(o1.image);
                }
            });
            initlist();
        } else if (id == R.id.action_sortbynamasurat){
            //Toast.makeText(getApplicationContext(), "Sort by Nama Surat", Toast.LENGTH_SHORT).show();
            Collections.sort(items, new Comparator<History>() {
                @Override
                public int compare(History o1, History o2) {
                    String sur1 = o1.from.toUpperCase();
                    String sur2 = o2.from.toUpperCase();
                    return sur1.compareTo(sur2);
                }
            });
            initlist();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initlist(){

        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LineItemDecoration(this, LinearLayout.VERTICAL));
        recyclerView.setHasFixedSize(true);

        //Collections.shuffle(items);


        //set data and list adapter
        mAdapter2 = new AdapterListHistory(this,items);
        recyclerView.setAdapter(mAdapter2);
        mAdapter2.notifyDataSetChanged();
        mAdapter2.setOnClickListener(new AdapterListHistory.OnClickListener() {
            @Override
            public void onItemClick(View view, History obj, int pos) {
                if(mAdapter2.getSelectedItemCount()>0){
                    enableActionMode(pos);
                }else{
                    History history = mAdapter2.getItem(pos);
                    showHistoryDialog(history);
                }
            }
        });
        //set data and list adapter
        /*mAdapter = new AdapterListInbox(this, items);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnClickListener(new AdapterListInbox.OnClickListener() {
            @Override
            public void onItemClick(View view, Inbox obj, int pos) {
                if (mAdapter.getSelectedItemCount() > 0) {
                    enableActionMode(pos);
                } else {
                    // read the inbox which removes bold from the row
                    Inbox inbox = mAdapter.getItem(pos);
                    showHistoryDialog(inbox);
                    //Toast.makeText(getApplicationContext(), "Read: " + inbox.from, Toast.LENGTH_SHORT).show();
                }
            }

           *//* @Override
            public void onItemLongClick(View view, Inbox obj, int pos) {
                enableActionMode(pos);
            }*//*
        });*/

        actionModeCallback = new ActionModeCallback();
        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshpull();
            }
        });
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        int count = mAdapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            Tools.setSystemBarColor(CodeHistory.this, R.color.blue_grey_700);
            mode.getMenuInflater().inflate(R.menu.menu_delete, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_delete) {
                deleteInboxes();
                mode.finish();
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.clearSelections();
            actionMode = null;
            Tools.setSystemBarColor(CodeHistory.this, R.color.red_600);
        }
    }

    private void deleteInboxes() {
        List<Integer> selectedItemPositions = mAdapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            mAdapter.removeData(selectedItemPositions.get(i));
        }
        mAdapter.notifyDataSetChanged();
    }

    private void showHistoryDialog(History inbox){
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.dialog_history, null);
        ((TextView) view.findViewById(R.id.name)).setText(inbox.from);
        ((TextView) view.findViewById(R.id.textview_status)).setText(inbox.email);
        ((TextView) view.findViewById(R.id.brief)).setText(inbox.date);
        ((TextView) view.findViewById(R.id.description)).setText(inbox.message);
        ((TextView) view.findViewById(R.id.description2)).setText(inbox.message2);
        (view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.hide();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // set background transparent
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

    }

    public void getHistory (){
        final TypedArray drw_arr = this.getResources().obtainTypedArray(R.array.status_surat);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                CodeConfig.URL_HISTORY+csp.getNik(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray JsHist = response.getJSONArray("result");
                            for(int i=0; i<JsHist.length();i++){
                                JSONObject result = JsHist.getJSONObject(i);
                                /*if (result.getString("status_kel").equals("0")) {
                                    value_notif++;
                                }*/

                                History obj = new History();
                                obj.from = result.getString("surat");
                                obj.email= result.getString("status_kel_ket");
                                obj.image = drw_arr.getResourceId(DataGenerator.berubah2(obj.email), -1);
                                obj.message = result.getString("peruntukan");
                                obj.message2 = DataGenerator.berubah(result.getString("status_kel_ket"),result.getString("pesan"));
                                obj.date = result.getString("tgl_surat");
                                obj.imageDrw = mcont.getResources().getDrawable(obj.image);
                                items.add(obj);
                            }
                            Collections.sort(items, new Comparator<History>() {
                                @Override
                                public int compare(History o1, History o2) {
                                    String date1 = o1.date.toUpperCase();
                                    String date2 = o2.date.toUpperCase();
                                    return date2.compareTo(date1);
                                }
                            });
                            initlist();
                            //Toast.makeText(mcont,"iki asline"+value_notif,Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcont,"Tidak bisa terhubung",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(mcont).add(jsonObjectRequest);

        /*preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("VALUE_NOTIF", String.valueOf(value_notif));
        editor.commit();*/

        /*for (int i = 0; i < 10; i++) {
            value_notif = value_notif+1;
        }*/

        //Toast.makeText(getBaseContext(), "iki preference"+csp.getValueNotif(), Toast.LENGTH_SHORT).show();
    }

    private void refreshpull(){
        swipeprog(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                items.clear();
                getHistory();
                swipeprog(false);
            }
        },4000);
    }

    private void swipeprog(final boolean show){
        if(!show){
            swipe_layout.setRefreshing(show);
            return;
        }
        swipe_layout.post(new Runnable() {
            @Override
            public void run() {
                swipe_layout.setRefreshing(show);
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), CodeAccount.class));
        finish();
        //super.onBackPressed();
    }
}

