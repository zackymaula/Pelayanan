package com.diskominfo.esuket.activity.main;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.model.Event;

import java.util.Hashtable;
import java.util.Map;

public class CodeAccountDetail extends AppCompatActivity {

    CodeSharedPreferenceHelper csp;
    Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_account_detail);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Upload data ke server ...");
        pDialog.setCancelable(false);

        setActionBar();
        setTextDetail();
        setOnClick();
    }

    private void setActionBar(){
        TextView mtext = (TextView) findViewById(R.id.textViewActionbarForm);
        mtext.setText("Informasi Akun");

        ((ImageButton) findViewById(R.id.buttonActionBarBackForm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setTextDetail(){
        ((TextView)findViewById(R.id.textViewAccountDetailNik)).setText(csp.getNik());
        ((TextView)findViewById(R.id.textViewAccountDetailNama)).setText(csp.getNama());
        ((TextView)findViewById(R.id.textViewAccountDetailJenisKelamin)).setText(csp.getKelamin());
        ((TextView)findViewById(R.id.textViewAccountDetailTempatTglLahir)).setText(csp.getTmpt_lhr()+", "+csp.getTgl_lhr());
        ((TextView)findViewById(R.id.textViewAccountDetailGolDarah)).setText(csp.getGol_drh());
        ((TextView)findViewById(R.id.textViewAccountDetailAlamat)).setText(csp.getAlamat());
        ((TextView)findViewById(R.id.textViewAccountDetailRtRw)).setText(csp.getRt()+" / "+csp.getRw());
        ((TextView)findViewById(R.id.textViewAccountDetailKelurahan)).setText(csp.getKel());
        ((TextView)findViewById(R.id.textViewAccountDetailKecamatan)).setText(csp.getKec());
        ((TextView)findViewById(R.id.textViewAccountDetailAgama)).setText(csp.getAgama());
        ((TextView)findViewById(R.id.textViewAccountDetailStatusPerkawinan)).setText(csp.getStat_kwn());
        ((TextView)findViewById(R.id.textViewAccountDetailPendidikan)).setText(csp.getPendidikan());
        ((TextView)findViewById(R.id.textViewAccountDetailPekerjaan)).setText(csp.getPekerjaan());
        ((TextView)findViewById(R.id.textViewAccountDetailNoHandphone)).setText(csp.getHandphone());
        //((TextView)findViewById(R.id.textViewAccountDetailEmail)).setText(csp.getEmail());
    }

    public void setOnClick(){
        LinearLayout layoutBtnHandphone = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountDetailHandphone);
        //LinearLayout layoutBtnEmail = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountDetailEmail);

        layoutBtnHandphone.setOnClickListener(layoutClick);
        //layoutBtnEmail.setOnClickListener(layoutClick);
    }

    View.OnClickListener layoutClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.linearLayoutButtonAccountDetailHandphone :
                    dialogNoHP("dialog_hp");
                    break;
                /*case R.id.linearLayoutButtonAccountDetailEmail :
                    dialogLogin("dialog_email");
                    break;*/
            }
        }
    };

    private void dialogNoHP(final String dialog_key) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_accountdetail);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final EditText editDialog = (EditText)dialog.findViewById(R.id.editTextDialogAccountDetail);

        if (dialog_key.equals("dialog_hp")) {
            editDialog.setText(csp.getHandphone());
        } /*else if (dialog_key.equals("dialog_email")) {
            editDialog.setText(csp.getEmail());
        }*/

        ((Button) dialog.findViewById(R.id.buttonDialogLoginSimpan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editDialog.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(),"Mohon diisi",Toast.LENGTH_LONG).show();
                } else {
                    preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
                    editor = preferences.edit();

                    if (dialog_key.equals("dialog_hp")) {
                        editor.putString("HANDPHONE", editDialog.getText().toString());
                        editor.commit();

                        Event event = new Event();
                        event.hp = editDialog.getText().toString();
                        //event.email = csp.getEmail();
                        displayDataResult(event);
                    } /*else if (dialog_key.equals("dialog_email")){
                        editor.putString("EMAIL", editDialog.getText().toString());
                        editor.commit();

                        Event event = new Event();
                        event.hp = csp.getHandphone();
                        event.email = editDialog.getText().toString();
                        displayDataResult(event);
                    }*/

                    dialog.dismiss();
                }
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void displayDataResult(Event event) {
        ((TextView)findViewById(R.id.textViewAccountDetailNoHandphone)).setText(event.hp);
        //((TextView)findViewById(R.id.textViewAccountDetailEmail)).setText(event.email);

        prosesUploadHP(event.hp);
    }

    //menampilkan progress dialog
    void showProgressDialog(){
        if(!pDialog.isShowing())pDialog.show();
    }

    //menutup progress dialog
    void dismissProgressDialog(){
        if(pDialog.isShowing())pDialog.dismiss();
    }

    public void prosesUploadHP(final String strNohp){
        showProgressDialog();
        StringRequest requestUpload = new StringRequest(Request.Method.POST, CodeConfig.URL_UPDATE_HP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Update no HP sukses",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Gagal update no HP",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<>();
                params.put("nik",csp.getNik());
                params.put("nohp",strNohp);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestUpload);
    }
}
