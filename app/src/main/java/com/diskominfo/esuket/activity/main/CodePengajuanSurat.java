package com.diskominfo.esuket.activity.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.activity.surat.*;
import com.diskominfo.esuket.adapter.AdapterGridShopCategory;
import com.diskominfo.esuket.data.DataGenerator;
import com.diskominfo.esuket.model.ShopCategory;
import com.diskominfo.esuket.utils.Tools;
import com.diskominfo.esuket.widget.SpacingItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CodePengajuanSurat extends AppCompatActivity {

    private BottomNavigationView navigation;
    //private View search_bar;

    private View parent_view;
    private RecyclerView recyclerView;
    private AdapterGridShopCategory mAdapter;

    private ImageButton imgBtnTentang,notif;
    private boolean show = false;
    private String pengumuman,kelurahan ;
    Context mcont;

    private static CodeSharedPreferenceHelper csp;
    Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pengajuan_surat);

        context = getApplicationContext();

        parent_view = findViewById(R.id.parent_view);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        imgBtnTentang = (ImageButton)findViewById(R.id.imageButtonTentang);
        notif = (ImageButton)findViewById(R.id.NotifPengumuman);
        mcont = this;
        /*
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)){
                Toast.makeText(this,"Camera should have Access",Toast.LENGTH_LONG).show();
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},1);
            }

        }else if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                Toast.makeText(this,"Folder should have Access",Toast.LENGTH_LONG).show();
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},2);
            }

        }*/
        //setValueNotif();
        getpengumuman();
        clickNavigation();
        btnClick();
    }

    public void btnClick(){
        imgBtnTentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*dialogTentang();*/
                if(!notif.isShown()){
                    Toast.makeText(getApplicationContext(),"Tidak ada pengumuman",Toast.LENGTH_LONG).show();
                }else{
                    dialogPengumuman();
                }
            }
        });
    }

    /*public void dialogTentang(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tentang);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.textViewVersiTentang)).setText("Version " + BuildConfig.VERSION_NAME);

        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogCloseTentang)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }*/

    public void dialogPengumuman(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pengumuman);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.content)).setText(pengumuman);
        ((TextView) dialog.findViewById(R.id.kelurahan)).setText(kelurahan);

        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void clickNavigation() {
        //search_bar = (View) findViewById(R.id.search_bar);
        //mTextMessage = (TextView) findViewById(R.id.search_text);

        CodeConfig.Permission(this);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    /*case R.id.navigation_dashboard:
                        //mTextMessage.setText(item.getTitle());
                        startActivity(new Intent(getBaseContext(), CodeDashboard.class));
                        finish();
                        return true;*/
                    case R.id.navigation_pengajuan_surat:
                        //mTextMessage.setText(item.getTitle());
                        return true;
                    case R.id.navigation_akun:
                        //mTextMessage.setText(item.getTitle());
                        startActivity(new Intent(getBaseContext(), CodeAccount.class));
                        finish();
                        return true;
                }
                return false;
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 8), true));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        List<ShopCategory> items = DataGenerator.getShoppingCategory(this);

        //set data and list adapter
        mAdapter = new AdapterGridShopCategory(this, items);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterGridShopCategory.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ShopCategory obj, int position) {
                //Snackbar.make(parent_view, "Item " + obj.title + " clicked", Snackbar.LENGTH_SHORT).show();
                if (position == 0){
                    //Surat Keterangan
                    startActivity(new Intent(getBaseContext(), CodeSketerangan.class));
                } else if (position == 1){
                    //SKTM
                    dialogSktm();
                } else if (position == 2){
                    //SKCK
                    startActivity(new Intent(getBaseContext(), CodeSkck.class));
                } /*else if (position == 3){
                    //Domisili
                    //startActivity(new Intent(getBaseContext(), CodeSkck.class));
                    Toast.makeText(getBaseContext(),"Surat Keterangan Domisili",Toast.LENGTH_SHORT).show();
                } else if (position == 4){
                    //Usaha
                    //startActivity(new Intent(getBaseContext(), CodeSkck.class));
                    Toast.makeText(getBaseContext(),"Surat Keterangan Usaha",Toast.LENGTH_SHORT).show();
                }*/ else if (position == 3){
                    //Surat Keterangan Belum Nikah
                    startActivity(new Intent(getBaseContext(), CodeSbelummenikah.class));
                } else /*if (position == 4){
                    //Surat Keterangan Boro Kerja
                    startActivity(new Intent(getBaseContext(), CodeSboro.class));
                } else*/ if (position == 4){
                    //KTP
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://penduduk-layanan.kedirikota.go.id/sakti/"));
                    startActivity(browserIntent);
                } else if (position == 5){
                    //KIA
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://penduduk-layanan.kedirikota.go.id/sakti/"));
                    startActivity(browserIntent);
                } else /*if (position == 6){
                    //KK
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://penduduk-layanan.kedirikota.go.id/sakti/"));
                    startActivity(browserIntent);
                } else */ if (position == 6){
                    //Kelahiran
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://penduduk-layanan.kedirikota.go.id/sakti/"));
                    startActivity(browserIntent);
                } else if (position == 7){
                    //Kematian
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://penduduk-layanan.kedirikota.go.id/sakti/"));
                    startActivity(browserIntent);
                }
            }
        });

        /*
        NestedScrollView nested_content = (NestedScrollView) findViewById(R.id.nested_scroll_view);
        nested_content.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY < oldScrollY) { // up
                    animateNavigation(false);
                    animateSearchBar(false);
                }
                if (scrollY > oldScrollY) { // down
                    animateNavigation(true);
                    animateSearchBar(true);
                }
            }
        });
        // display image
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_1), R.drawable.image_8);
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_2), R.drawable.image_9);
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_3), R.drawable.image_15);
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_4), R.drawable.image_14);
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_5), R.drawable.image_12);
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_6), R.drawable.image_2);
        Tools.displayImageOriginal(this, (ImageView) findViewById(R.id.image_7), R.drawable.image_5);


        ((ImageButton) findViewById(R.id.bt_menu)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);*/
    }


    /*boolean isNavigationHide = false;

    private void animateNavigation(final boolean hide) {
        if (isNavigationHide && hide || !isNavigationHide && !hide) return;
        isNavigationHide = hide;
        int moveY = hide ? (2 * navigation.getHeight()) : 0;
        navigation.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    boolean isSearchBarHide = false;

    private void animateSearchBar(final boolean hide) {
        if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
        isSearchBarHide = hide;
        int moveY = hide ? -(2 * search_bar.getHeight()) : 0;
        search_bar.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }*/

    //dialog SKTM
    private void dialogSktm() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_surat_sktm);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogSktmIndividu)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getBaseContext(), CodeSktmIndividu.class));
                dialog.dismiss();
            }
        });

        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogSktmSekolah)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getBaseContext(), CodeSktmSekolah.class));
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    /*@Override
    public void onBackPressed() {
        dialogExit();
        //super.onBackPressed();
    }

    //dialog SKTM
    private void dialogExit() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogExitYa)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
            }
        });

        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogExitTidak)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }*/
    /*@Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1 :{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,"Camera Access Granted",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this,"You don't have access to camera",Toast.LENGTH_LONG).show();
                }
                return;
            }
            case 2:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,"Folder Access Granted",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this,"You don't have access to folder",Toast.LENGTH_LONG).show();
                }
                return;
            }
        }

    }*/

    public void getpengumuman(){
        csp = new CodeSharedPreferenceHelper(mcont);
        String replaceKel = csp.getKel().replace(" ","%20");
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                CodeConfig.URL_PENGUMUMAN+replaceKel,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jspeng = response.getJSONArray("result");
                            JSONObject pengcontent = jspeng.getJSONObject(0);
                                pengumuman = pengcontent.getString("pesan");
                                kelurahan = pengcontent.getString("nama_kel");
                                show = convertbol(pengcontent.getString("status_tampil"));
                                initNotif();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcont,"Tidak bisa terhubung",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(mcont).add(jsonObjectRequest);
        initNotif();
    }

    public void initNotif(){
        if(show==true){
            notif.setVisibility(View.VISIBLE);
            dialogPengumuman();
        }else {
            notif.setVisibility(View.GONE);
        }
    }

    public boolean convertbol(String tampil){
        boolean tmpl;
        if(tampil.equals("1")){
            tmpl=true;
        }else{
            tmpl=false;
        }
        return tmpl;
    }

    public void insertNoHandphone () {

    }

    /*public void setValueNotif(){
        preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("VALUE_NOTIF", "0");
        editor.commit();
    }*/
}
