package com.diskominfo.esuket.activity.main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.diskominfo.esuket.R;

public class CodeSplashScreen extends AppCompatActivity {

    /** Duration of wait **/
    //private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash_screen);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        /*new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                *//* Create an Intent that will start the Menu-Activity. *//*
                Intent mainIntent = new Intent(CodeSplashScreen.this,CodeLogin.class);
                CodeSplashScreen.this.startActivity(mainIntent);
                CodeSplashScreen.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);*/
    }
}
