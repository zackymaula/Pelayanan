package com.diskominfo.esuket.activity.assets;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Zacky on 04/04/2018.
 */

public class CodeSharedPreferenceHelper {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    /*String nik,
            nama,
            kelamin,
            tmpt_lhr,
            tgl_lhr,
            gol_drh,
            agama,
            stat_kwn,
            pendidikan,
            pekerjaan,
            alamat,
            rt,
            rw,
            kel,
            kec,
            nik_ibu,
            nama_ibu,
            nik_ayah,
            nama_ayah;*/

    public CodeSharedPreferenceHelper(Context context, Activity activity,
                                    String nik,
                                    String nama,
                                    String kelamin,
                                    String tmpt_lhr,
                                    String tgl_lhr,
                                    String gol_drh,
                                    String agama,
                                    String stat_kwn,
                                    String pendidikan,
                                    String pekerjaan,
                                    String alamat,
                                    String rt,
                                    String rw,
                                    String kel,
                                    String kec,
                                    String nik_ibu,
                                    String nama_ibu,
                                    String nik_ayah,
                                    String nama_ayah,
                                    String nokk,
                                    String kdpos,
                                    String noprop,
                                    String nokel,
                                    String nokec) {
        preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
        editor = this.preferences.edit();
        editor.putString("NIK", nik);
        editor.putString("NAMA_LGKP", nama);
        editor.putString("JENIS_KLMN", kelamin);
        editor.putString("TMPT_LHR", tmpt_lhr);
        editor.putString("TGL_LHR", tgl_lhr);
        editor.putString("GOL_DRH", gol_drh);
        editor.putString("AGAMA", agama);
        editor.putString("STAT_KWN", stat_kwn);
        editor.putString("PENDIDIKAN", pendidikan);
        editor.putString("PEKERJAAN", pekerjaan);
        editor.putString("ALAMAT", alamat);
        editor.putString("RT", rt);
        editor.putString("RW", rw);
        editor.putString("KELURAHAN", kel);
        editor.putString("KECAMATAN", kec);
        editor.putString("NIK_IBU", nik_ibu);
        editor.putString("NAMA_LGKP_IBU", nama_ibu);
        editor.putString("NIK_AYAH", nik_ayah);
        editor.putString("NAMA_LGKP_AYAH", nama_ayah);
        editor.putString("NOKK", nokk);
        editor.putString("KDPOS", kdpos);
        editor.putString("NOPROP", noprop);
        editor.putString("NOKEL", nokel);
        editor.putString("NOKEC", nokec);
        editor.commit();
    }

    public CodeSharedPreferenceHelper(Context context){
        this.preferences = context.getSharedPreferences("NIKpref",Context.MODE_PRIVATE);
    }

    public String getNik() {
        return preferences.getString("NIK","");
    }

    public String getNama() {
        return preferences.getString("NAMA_LGKP","");
    }

    public String getKelamin() {
        return preferences.getString("JENIS_KLMN","");
    }

    public String getTmpt_lhr() {
        return preferences.getString("TMPT_LHR","");
    }

    public String getTgl_lhr() {
        return preferences.getString("TGL_LHR","");
    }

    public String getGol_drh() {
        return preferences.getString("GOL_DRH","");
    }

    public String getAgama() {
        return preferences.getString("AGAMA","");
    }

    public String getStat_kwn() {
        return preferences.getString("STAT_KWN","");
    }

    public String getPendidikan() {
        return preferences.getString("PENDIDIKAN","");
    }

    public String getPekerjaan() {
        return preferences.getString("PEKERJAAN","");
    }

    public String getAlamat() {
        return preferences.getString("ALAMAT","");
    }

    public String getRt() {
        return preferences.getString("RT","");
    }

    public String getRw() {
        return preferences.getString("RW","");
    }

    public String getKel() {
        return preferences.getString("KELURAHAN","");
    }

    public String getKec() {
        return preferences.getString("KECAMATAN","");
    }

    public String getNik_ibu() {
        return preferences.getString("NIK_IBU","");
    }

    public String getNik_ayah() {
        return preferences.getString("NIK_AYAH","");
    }

    public String getNama_ayah() {
        return preferences.getString("NAMA_LGKP_AYAH","");
    }

    public String getNama_ibu() {
        return preferences.getString("NAMA_LGKP_IBU","");
    }

    public String getNoKK() {
        return preferences.getString("NOKK","");
    }

    public String getKdPos() {
        return preferences.getString("KDPOS","");
    }

    public String getNoProp() {
        return preferences.getString("NOPROP","");
    }

    public String getNoKel() {
        return preferences.getString("NOKEL","");
    }

    public String getNoKec() {
        return preferences.getString("NOKEC","");
    }


    public String getHandphone() {
        return preferences.getString("HANDPHONE","");
    }

    /*public String getEmail() {
        return preferences.getString("EMAIL","");
    }*/

    public String getValueNotif() {
        return preferences.getString("VALUE_NOTIF","");
    }

    /*public String getNama_ibu() {
        return nama_ibu = preferences.getString("NAMA_LGKP_IBU","");
    }*/
}
