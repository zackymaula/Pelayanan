package com.diskominfo.esuket.activity.surat;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.diskominfo.esuket.R;
import com.diskominfo.esuket.utils.Tools;
import com.diskominfo.esuket.adapter.AdapterListBasic;
import com.diskominfo.esuket.model.People;
import com.diskominfo.esuket.data.DataGenerator;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class CodeSboro extends AppCompatActivity {

    private List<View> view_list = new ArrayList<>();
    private int MAX_STEP ;
    private int current_step = 1;
    private TextView mtext;

    private List<People> daftar = new ArrayList<>();

    private View parent_view;
    private RecyclerView recyclerView;
    private AdapterListBasic mAdapter;
    private EditText nikikut,ketikut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sboro);

        nikikut = (EditText) findViewById(R.id.TextView_NikIkut);
        ketikut = (EditText) findViewById(R.id.TextView_KetIkut);


        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        mtext = (TextView) findViewById(R.id.textViewActionbarForm);
        mtext.setText("SURAT BORO KERJA");

        ((ImageButton) findViewById(R.id.buttonActionBarBackForm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initComponent() {

        view_list.add(findViewById(R.id.ln_borodatapenduduk));
        view_list.add(findViewById(R.id.ln_boropengikut));
        view_list.add(findViewById(R.id.ln_boroketerangan));

        MAX_STEP = view_list.size();

        for(View v:view_list){
            v.setVisibility(View.GONE);
        }

        ((ImageButton) findViewById(R.id.tambah_pengikut_boro)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nikikut.getText().toString().isEmpty()){
                    Toast.makeText(getBaseContext(),"Nik Harap Diisi",Toast.LENGTH_LONG).show();
                } else if(ketikut.getText().toString().isEmpty()){
                    Toast.makeText(getBaseContext(),"Keterangan Harap Diisi",Toast.LENGTH_LONG).show();
                }else{
                    People obj = new People();
                    obj.name = nikikut.getText().toString();
                    obj.descript = ketikut.getText().toString();

                    daftar.add(obj);
                    nikikut.setText("");
                    ketikut.setText("");
                }
            }
        });

        ((LinearLayout) findViewById(R.id.lyt_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backStep(current_step);
                bottomProgressDots(current_step);
            }
        });

        ((LinearLayout) findViewById(R.id.lyt_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextStep(current_step);
                bottomProgressDots(current_step);
            }
        });
        view_list.get(current_step-1).setVisibility(View.VISIBLE);

        ((EditText) findViewById(R.id.date_mulai)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDatePickerDark((EditText) v);
            }
        });

        //((TextView) findViewById(R.id.date_mulai)).setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
            //    dialogDatePickerDark((TextView) v);
           // }
        //});
        ((EditText) findViewById(R.id.date_selesai)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDatePickerDark((EditText) v);
            }
        });
        bottomProgressDots(current_step);
        initList();
    }

    private void nextStep(int progress) {
        if (progress < MAX_STEP) {
            progress++;
            view_list.get(current_step-1).setVisibility(View.GONE);
            current_step = progress;
        }
        view_list.get(current_step-1).setVisibility(View.VISIBLE);
    }

    private void backStep(int progress) {
        if (progress > 1) {
            progress--;
            view_list.get(current_step-1).setVisibility(View.GONE);
            current_step = progress;
        }
        view_list.get(current_step-1).setVisibility(View.VISIBLE);
    }

    private void bottomProgressDots(int current_index) {
        current_index--;
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        }
    }

    private void dialogDatePickerDark(final TextView tv) {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        tv.setText(Tools.getFormattedDateSimple(date_ship_millis));
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark theme
        datePicker.setThemeDark(true);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.setMinDate(cur_calender);
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

    private void initList(){
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        //List<People> items = DataGenerator.getPeopleData(this);
        //items.addAll(DataGenerator.getPeopleData(this));
        //items.addAll(DataGenerator.getPeopleData(this));

        //List<People> items = nikdata();
        //items.addAll(nikdata());
        //items.addAll(DataGenerator.getPeopleData(this));

        //set data and list adapter
        mAdapter = new AdapterListBasic(this, daftar);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterListBasic.OnItemClickListener() {
            @Override
            public void onItemClick(View view, People obj, int position) {
                Snackbar.make(parent_view, "Item " + obj.name + " clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    //public static void nikdata(){


        //daftar.add(obj);

        //Collections.shuffle(daftar);
        //return daftar;
    //}


}
