package com.diskominfo.esuket.activity.main;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.activity.surat.CodeSbelummenikah;
import com.diskominfo.esuket.activity.surat.CodeSkck;
import com.diskominfo.esuket.activity.surat.CodeSketerangan;
import com.diskominfo.esuket.activity.surat.CodeSktmIndividu;
import com.diskominfo.esuket.activity.surat.CodeSktmSekolah;
import com.diskominfo.esuket.utils.ViewAnimation;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class CodeLogin extends AppCompatActivity {

    //private View parent_view;
    private final static int LOADING_DURATION = 2500;

    //private RecyclerView recyclerView;
    //private AdapterListFolderFile mAdapter;
    private RelativeLayout layoutLogin;
    private Button btnLogin;
    private EditText editNIK, editKK;
    private ImageView imgLogo;

    ProgressDialog pDialog;

    public String
            strNik = "",
            strNama = "",
            strJnsKelamin = "",
            strTmptLahir = "",
            strTglLahir = "",
            strGolDarah = "",
            strAgama = "",
            strStatusKwn = "",
            strPendidikan = "",
            strPekerjaan = "",

            strAlamat = "",
            strRT = "",
            strRW = "",
            strKelurahan = "",
            strKecamatan = "",
            strNikIbu = "",
            strNamaIbu = "",
            strNikAyah = "",
            strNamaAyah = "",
            strKk = "",

            strKodePos = "",
            strNoProp = "",
            strNoKel = "",
            strNoKec = "",

            strNohp = "";

    CodeSharedPreferenceHelper codeSharedPreferenceHelper, csp;
    Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    //ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Upload data ke server ...");
        pDialog.setCancelable(false);
        //parent_view = findViewById(android.R.id.content);
        //recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        layoutLogin = (RelativeLayout) this.findViewById(R.id.layoutRelativeLogin);
        btnLogin = (Button)findViewById(R.id.buttonLogin);
        editNIK = (EditText)findViewById(R.id.editTextLoginNIK);
        editKK = (EditText)findViewById(R.id.editTextLoginKK);
        imgLogo = (ImageView)findViewById(R.id.imageViewLoginLogoPemkot);

        loadingSplash();
        onClick();
    }

    private void loadingSplash() {
        final LinearLayout lyt_progress = (LinearLayout) findViewById(R.id.lyt_progress);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);
        //recyclerView.setVisibility(View.GONE);
        layoutLogin.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewAnimation.fadeOut(lyt_progress);
            }
        }, LOADING_DURATION);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(csp.getNik()==""){
                    initComponent();
                } else {
                    startActivity(new Intent(getBaseContext(), CodePengajuanSurat.class));
                    //startActivity(new Intent(getBaseContext(), CodeAccount.class));
                    finish();
                }
            }
        }, LOADING_DURATION + 400);
    }

    private void initComponent() {
        //Toast.makeText(getApplicationContext(), "After Progress", Toast.LENGTH_SHORT).show();
        layoutLogin.setVisibility(View.VISIBLE);
    }

    public void onClick(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getBaseContext(), CodePengajuanSurat.class));
                if (editNIK.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(),"NIK masih kosong",Toast.LENGTH_SHORT).show();
                } else if (editKK.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(),"Nomor KK masih kosong",Toast.LENGTH_SHORT).show();
                } else {
                    int socketTimeout = 10000;//10 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            CodeConfig.URL_NIK+editNIK.getText().toString().trim(),
                            null,
                            jsonObjectListener,
                            jsonErrorListener
                    );
                    jsonObjectRequest.setRetryPolicy(policy);
                    Volley.newRequestQueue(CodeLogin.this).add(jsonObjectRequest);
                }
            }
        });

        imgLogo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startActivity(new Intent(getBaseContext(), CodePengajuanSurat.class));
                //startActivity(new Intent(getBaseContext(), CodeSkck.class));
                finish();
                return false;
            }
        });
    }

    public void getNoHP(){
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                CodeConfig.URL_CEK_HP + strNik,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray item = response.getJSONArray("result");
                            JSONObject person = item.getJSONObject(0);
                            //nohp = person.getString("nohp");

                            preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
                            editor = preferences.edit();
                            editor.putString("HANDPHONE", person.getString("nohp"));
                            editor.putString("VALUE_NOTIF", "0");
                            editor.commit();

                            setSharedPref();

                            startActivity(new Intent(getBaseContext(), CodePengajuanSurat.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(getBaseContext(),"No HP belum ada",Toast.LENGTH_SHORT).show();
                            dialogLogin(strNama);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(),"Tidak ada koneksi internet",Toast.LENGTH_SHORT).show();
                    }
                }
        );
        jsonObjectRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(CodeLogin.this).add(jsonObjectRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_refresh) {
            loadingSplash();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    //proses login get JSON
    com.android.volley.Response.Listener<JSONObject> jsonObjectListener = new com.android.volley.Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            //dismissProgressDialog();
            try {
                JSONArray item = response.getJSONArray("item");
                JSONObject person = item.getJSONObject(0);

                strNik = person.getString("NIK");
                strNama = person.getString("NAMA_LGKP");
                strJnsKelamin = person.getString("JENIS_KLMN");
                strTmptLahir = person.getString("TMPT_LHR");
                strTglLahir = person.getString("TGL_LHR");
                strGolDarah = person.getString("GOL_DRH");
                strAgama = person.getString("AGAMA");
                strStatusKwn = person.getString("STAT_KWN");
                strPendidikan = person.getString("PENDIDIKAN");
                strPekerjaan = person.getString("PEKERJAAN");

                strAlamat = person.getString("ALAMAT");
                strRT = person.getString("RT");
                strRW = person.getString("RW");
                strKelurahan = person.getString("KELURAHAN");
                strKecamatan = person.getString("KECAMATAN");
                strNikIbu = person.getString("NIK_IBU");
                strNamaIbu = person.getString("NAMA_LGKP_IBU");
                strNikAyah = person.getString("NIK_AYAH");
                strNamaAyah = person.getString("NAMA_LGKP_AYAH");
                strKk = person.getString("NOKK");

                strKodePos = person.getString("KDPOS");
                strNoProp = person.getString("NOPROP");
                strNoKel = person.getString("NOKEL");
                strNoKec = person.getString("NOKEC");

                /*codeSharedPreferenceHelper = new CodeSharedPreferenceHelper(
                        context, CodeLogin.this,
                        person.getString("NIK"),
                        person.getString("NAMA_LGKP"),
                        person.getString("JENIS_KLMN") ,
                        person.getString("TMPT_LHR"),
                        person.getString("TGL_LHR"),
                        person.getString("GOL_DRH"),
                        person.getString("AGAMA"),
                        person.getString("STAT_KWN"),
                        person.getString("PENDIDIKAN"),
                        person.getString("PEKERJAAN"),
                        person.getString("ALAMAT"),
                        person.getString("RT"),
                        person.getString("RW"),
                        person.getString("KELURAHAN"),
                        person.getString("KECAMATAN"),
                        person.getString("NIK_IBU"),
                        person.getString("NAMA_LGKP_IBU"),
                        person.getString("NIK_AYAH"),
                        person.getString("NAMA_LGKP_AYAH"),
                        person.getString("NOKK"),
                        person.getString("KDPOS"),
                        person.getString("NOPROP"),
                        person.getString("NOKEL"),
                        person.getString("NOKEC")
                );*/

                compareKK();

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(),"Maaf, NIK tidak terdaftar",Toast.LENGTH_SHORT).show();
            }
        }
    };

    com.android.volley.Response.ErrorListener jsonErrorListener = new com.android.volley.Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getBaseContext(),"Tidak ada koneksi internet",Toast.LENGTH_SHORT).show();
        }
    };

    public void compareKK(){
        if (!editKK.getText().toString().trim().equals(strKk)) {
            Toast.makeText(getBaseContext(), "Maaf, KK tidak benar", Toast.LENGTH_SHORT).show();
            layoutLogin.setAlpha(1.0f);
        } else {
            getNoHP();
        }
    }

    //menampilkan progress dialog
    void showProgressDialog(){
        if(!pDialog.isShowing())pDialog.show();
    }

    //menutup progress dialog
    void dismissProgressDialog(){
        if(pDialog.isShowing())pDialog.dismiss();
    }

    public void prosesUploadHP(){
        showProgressDialog();
        StringRequest requestUpload = new StringRequest(Request.Method.POST, CodeConfig.URL_INSERT_HP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"No HP tersimpan",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Gagal simpan no HP",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<>();
                params.put("nik",strNik);
                params.put("nohp",strNohp);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestUpload);
    }

    private void dialogLogin(String namaDialog) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView)dialog.findViewById(R.id.textViewDialogLoginNama)).setText(namaDialog);
        final EditText editHandphone = (EditText)dialog.findViewById(R.id.editTextDialogLoginHandphone);
        //final EditText editEmail = (EditText)dialog.findViewById(R.id.editTextDialogLoginEmail);

        ((Button) dialog.findViewById(R.id.buttonDialogLoginSimpan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editHandphone.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(),"No. Handphone masih kosong",Toast.LENGTH_LONG).show();
                }/* else if (editEmail.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(),"Email masih kosong",Toast.LENGTH_LONG).show();
                } */else {
                    strNohp = editHandphone.getText().toString();
                    preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
                    editor = preferences.edit();
                    editor.putString("HANDPHONE", editHandphone.getText().toString());
                    editor.putString("VALUE_NOTIF", "0");
                    editor.commit();
                    startActivity(new Intent(getBaseContext(), CodePengajuanSurat.class));
                    dialog.dismiss();

                    prosesUploadHP();
                    setSharedPref();
                }
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void setSharedPref(){
        codeSharedPreferenceHelper = new CodeSharedPreferenceHelper(
                context, CodeLogin.this,
                strNik,
                strNama,
                strJnsKelamin,
                strTmptLahir,
                strTglLahir,
                strGolDarah,
                strAgama,
                strStatusKwn,
                strPendidikan,
                strPekerjaan,

                strAlamat,
                strRT,
                strRW,
                strKelurahan,
                strKecamatan,
                strNikIbu,
                strNamaIbu,
                strNikAyah,
                strNamaAyah,
                strKk,

                strKodePos,
                strNoProp,
                strNoKel,
                strNoKec
        );
    }
}
