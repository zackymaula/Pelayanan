package com.diskominfo.esuket.activity.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.diskominfo.esuket.BuildConfig;
import com.diskominfo.esuket.R;
import com.diskominfo.esuket.activity.assets.CodeConfig;
import com.diskominfo.esuket.activity.assets.CodeSharedPreferenceHelper;
import com.diskominfo.esuket.data.DataGenerator;
import com.diskominfo.esuket.model.History;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

public class CodeAccount extends AppCompatActivity {

    CodeSharedPreferenceHelper csp;
    Context context;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    /*TextView txtNama,
            txtNIK,
            txtTempatLahir,
            txtTanggalLahir,
            txtJenisKelamin,
            txtRTRW,
            txtKelurahan,
            txtKecamatan,
            txtAgama,
            txtStatusKawin,
            txtPekerjaan;*/

    private BottomNavigationView navigation;
    int jum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_account);

        context = getApplicationContext();
        csp = new CodeSharedPreferenceHelper(context);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.getMenu().findItem(R.id.navigation_akun).setChecked(true);

        setTextView();
        clickNavigation();
        onClick();
        notifHistory();
        //((TextView)findViewById(R.id.jmlh_surat)).setText(jum);
    }

    public void setTextView(){
        ((TextView)findViewById(R.id.textViewAccountNama)).setText(csp.getNama());
        ((TextView)findViewById(R.id.textViewAccountNIK)).setText(csp.getNik());
        /*((TextView)findViewById(R.id.textViewAccountTempatLahir)).setText(csp.getTmpt_lhr());
        ((TextView)findViewById(R.id.textViewAccountTanggalLahir)).setText(csp.getTgl_lhr());
        ((TextView)findViewById(R.id.textViewAccountJenisKelamin)).setText(csp.getKelamin());
        ((TextView)findViewById(R.id.textViewAccountRTRW)).setText(csp.getRt()+"/"+csp.getRw());
        ((TextView)findViewById(R.id.textViewAccountKelurahan)).setText(csp.getKel());
        ((TextView)findViewById(R.id.textViewAccountKecamatan)).setText(csp.getKec());
        ((TextView)findViewById(R.id.textViewAccountAgama)).setText(csp.getAgama());
        ((TextView)findViewById(R.id.textViewAccountStatusKawin)).setText(csp.getStat_kwn());
        ((TextView)findViewById(R.id.textViewAccountPekerjaan)).setText(csp.getPekerjaan());
        ((TextView)findViewById(R.id.textViewAccountHandphone)).setText(csp.getHandphone());
        ((TextView)findViewById(R.id.textViewAccountEmail)).setText(csp.getEmail());*/
    }

    public void onClick(){
        /*Button btnHistory = (Button)findViewById(R.id.buttonHistoryPengajuanSurat);
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), CodeHistory.class));
            }
        });*/
        LinearLayout layoutBtnInformasi = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountInformasi);
        //LinearLayout layoutBtnMessage = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountMessage);
        LinearLayout layoutBtnHistori = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountHistori);
        LinearLayout layoutBtnAbout = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountAbout);
        LinearLayout layoutBtnLogout = (LinearLayout)findViewById(R.id.linearLayoutButtonAccountLogout);

        layoutBtnInformasi.setOnClickListener(layoutClick);
        //layoutBtnMessage.setOnClickListener(layoutClick);
        layoutBtnHistori.setOnClickListener(layoutClick);
        layoutBtnAbout.setOnClickListener(layoutClick);
        layoutBtnLogout.setOnClickListener(layoutClick);
    }

    View.OnClickListener layoutClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.linearLayoutButtonAccountInformasi :
                    startActivity(new Intent(getBaseContext(), CodeAccountDetail.class));
                    break;
                /*case R.id.linearLayoutButtonAccountMessage :
                    Toast.makeText(getBaseContext(), "Pesan", Toast.LENGTH_SHORT).show();
                    break;*/
                case R.id.linearLayoutButtonAccountHistori :
                    startActivity(new Intent(getBaseContext(), CodeHistory.class));
                    preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
                    editor = preferences.edit();
                    editor.putString("VALUE_NOTIF", String.valueOf(jum));
                    editor.commit();
                    finish();
                    break;
                case R.id.linearLayoutButtonAccountAbout :
                    dialogTentang();
                    break;
                case R.id.linearLayoutButtonAccountLogout :
                    dialogExit();
                    break;
            }
        }
    };

    public void destroyShared(){
        preferences = context.getSharedPreferences("NIKpref", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    private void clickNavigation() {
        //search_bar = (View) findViewById(R.id.search_bar);
        //mTextMessage = (TextView) findViewById(R.id.search_text);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    /*case R.id.navigation_dashboard:
                        //mTextMessage.setText(item.getTitle());
                        startActivity(new Intent(getBaseContext(), CodeDashboard.class));
                        finish();
                        return true;*/
                    case R.id.navigation_pengajuan_surat:
                        //mTextMessage.setText(item.getTitle());
                        startActivity(new Intent(getBaseContext(), CodePengajuanSurat.class));
                        finish();
                        return true;
                    case R.id.navigation_akun:
                        //mTextMessage.setText(item.getTitle());
                        return true;
                }
                return false;
            }
        });
    }

    /*@Override
    public void onBackPressed() {
        dialogExit();
        //super.onBackPressed();
    }*/

    public void dialogTentang(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tentang);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.textViewVersiTentang)).setText("Version " + BuildConfig.VERSION_NAME);

        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogCloseTentang)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void dialogExit() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogExitYa)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                destroyShared();
                finish();
                Intent i = new Intent(getBaseContext(), CodeLogin.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        ((AppCompatButton) dialog.findViewById(R.id.buttonDialogExitTidak)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void notifHistory(){
        csp = new CodeSharedPreferenceHelper(getApplicationContext());
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                CodeConfig.URL_HISTORY+csp.getNik(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray JsHist = response.getJSONArray("result");
                            for(int i=0; i<JsHist.length();i++){
                                JSONObject result = JsHist.getJSONObject(i);
                                if(result.getString("status_kel").equals("0")){
                                    jum++;
                                }
                            }
                            tampilteks();
                            //((TextView) findViewById(R.id.jmlh_surat)).setText("*"+csp.getValueNotif());
                            //Toast.makeText(getBaseContext(), ""+jum, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Tidak bisa terhubung",Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

    public void tampilteks(){
        int hasil = Math.abs(jum-Integer.parseInt(csp.getValueNotif()));
        if(hasil==0){
            ((TextView) findViewById(R.id.jmlh_surat)).setVisibility(View.GONE);
        }else{
            ((TextView) findViewById(R.id.jmlh_surat)).setText("*"+hasil);
        }
    }
}
