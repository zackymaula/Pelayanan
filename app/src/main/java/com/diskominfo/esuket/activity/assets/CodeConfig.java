package com.diskominfo.esuket.activity.assets;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.diskominfo.esuket.BuildConfig;

import java.util.ArrayList;
import java.util.List;


public class CodeConfig extends Activity {

    //static String ip_nik = "http://srv.suket.kedirikota.go.id/";
    //static String ip_main = "http://esuket.kedirikota.go.id/";
    //static String ip_main = "http://192.168.0.105/esuket-clone/";

    public static final String URL_NIK = BuildConfig.IP_NIK+"api/cek/";
    public static final String URL_INSERT_SKCK = BuildConfig.IP_MAIN+"warga_mobile_skck/insert_surat";
    public static final String URL_INSERT_SKETERANGAN = BuildConfig.IP_MAIN+"warga_mobile_sketerangan/insert_surat";
    public static final String URL_INSERT_SBELUMNIKAH = BuildConfig.IP_MAIN+"warga_mobile_sbelumnikah/insert_surat";
    public static final String URL_INSERT_SKTM_INDIVIDU = BuildConfig.IP_MAIN+"warga_mobile_sktm/insert_surat_individu";
    public static final String URL_INSERT_SKTM_SEKOLAH = BuildConfig.IP_MAIN+"warga_mobile_sktm/insert_surat_sekolah";
    public static final String URL_HISTORY = BuildConfig.IP_MAIN+"warga_mobile_history/get_nik/";
    public static final String URL_PENGUMUMAN = BuildConfig.IP_MAIN+"warga_mobile_pengumuman/get_kel/";
    public static final String URL_CEK_HP = BuildConfig.IP_MAIN+"warga_mobile_akun/cek_hp/";
    public static final String URL_INSERT_HP = BuildConfig.IP_MAIN+"warga_mobile_akun/insert_hp";
    public static final String URL_UPDATE_HP = BuildConfig.IP_MAIN+"warga_mobile_akun/update_hp";

    /*public static final String URL_INSERT_SKCK = ip_main+"warga_mobile_skck/insert_surat";
    public static final String URL_INSERT_SKETERANGAN = ip_main+"warga_mobile_sketerangan/insert_surat";
    public static final String URL_INSERT_SBELUMNIKAH = ip_main+"warga_mobile_sbelumnikah/insert_surat";
    public static final String URL_INSERT_SKTM_INDIVIDU = ip_main+"warga_mobile_sktm/insert_surat_individu";
    public static final String URL_INSERT_SKTM_SEKOLAH = ip_main+"warga_mobile_sktm/insert_surat_sekolah";
    public static final String URL_HISTORY = ip_main+"warga_mobile_history/get_nik/";
    public static final String URL_PENGUMUMAN = ip_main+"warga_mobile_pengumuman/get_kel/";
    public static final String URL_CEK_HP = ip_main+"warga_mobile_akun/cek_hp/";
    public static final String URL_INSERT_HP = ip_main+"warga_mobile_akun/insert_hp";
    public static final String URL_UPDATE_HP = ip_main+"warga_mobile_akun/update_hp";*/

    static Context cont;

    public static final void Permission (Context kot) {
        cont = kot;
        Activity act = (Activity) cont;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int permissionCAMERA = ContextCompat.checkSelfPermission(cont,Manifest.permission.CAMERA);
            int storagePermission = ContextCompat.checkSelfPermission(cont,Manifest.permission.WRITE_EXTERNAL_STORAGE);

            List<String> listPermissionsNeeded = new ArrayList<>();

            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(act,listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),1);
            }
        } else {
            //Toast.makeText(kot, ""+Build.VERSION.SDK_INT, Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1 :{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(cont,"Access Granted",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(cont,"You don't have access",Toast.LENGTH_LONG).show();
                }
                return;
            }
        }

    }

}
